# bussiness-pitch

# 1. Menampilkan use case table untuk produk digitalnya

UseCase User
1. Pencarian dan Pembelian Produk: User dapat mencari produk yang diinginkan. User dapat melihat detail produk, termasuk harga, deskripsi. User dapat memilih untuk membeli produk tersebut dan menyelesaikan pembayaran menggunakan berbagai metode pembayaran yang disediakan.
2. Keranjang: User dapat melihat barang yang ada dikeranjang dan bisa menambahkan ke order

UseCase Seller
1. Membuka Toko Online: Penjual dapat mengatur profil toko, mengunggah foto dan deskripsi produk.
2. Menerima Pesanan dan Memproses Pembayaran: Ketika penjual menerima pesanan dari pelanggan, Penjual dapat melihat daftar pesanan, mengonfirmasi pembayaran, dan mempersiapkan pesanan untuk dikirim.

# 2. Web service (CRUD) dari produk digitalnya

![server](/uploads/e7905ee37d6b0bf2f2dc87617bb0a77b/server.gif)

# 3. Koneksi ke database

![db](/uploads/3f7709a5323bb93956ab70295bbad84a/db.png)

# 4. Visualisasi data untuk business intelligence produk digital

![BI](/uploads/ded3a2c6b14d363bbba1cf202e5b951d/BI.png)

# 5. Built-in function dengan ketentuan
- Regex
![regex](/uploads/a3081119c5fae84bb2f5b51761a370e4/regex.png)
- Substring dan Casting

# 6. Penggunaan Subquery pada produk digital

# 7. Penggunaan Transaction pada produk digital

![transaksi](/uploads/012f0125cfb39e2f88b57649bae0ec04/transaksi.gif)

# 8. Penggunaan Procedure / Function dan Trigger pada produk digital

- Procedure dan function

- Triggers

# 9. Demonstrasin Data Control Language (DCL) pada produk digital

# 10. Constraint yang digunakan pada produk digital

- indexs

![indexes](/uploads/87c0431ea686040de6153b7ff7e042e9/indexes.gif)

- unique key

![uniq](/uploads/cc3de339d10058d74d5daf1ffb4176e5/uniq.gif)

- foreign key

![fk](/uploads/4a2586620bd6c5cda705f0ad1d88b9ac/fk.gif)

# 11. Demonstrasi produk digital (Youtube)

# 12. Demonstrasikan UI untuk CRUDnya

![ui](/uploads/1acf52cbeb19dce3957459f7d4f90a56/ui.gif)
